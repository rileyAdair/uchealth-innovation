import stickyHeader from './modules/stickyHeader';
import proofSlider from './modules/proofSlider';
import areasSlider from './modules/areasSlider';
import youtubePlayer from './modules/youtubePlayer';

function home() {
  stickyHeader.init();
  proofSlider.init();
  areasSlider.init();
  youtubePlayer.init();
}

export default home();
