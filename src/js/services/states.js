const states = {
  userAgent: window.navigator.userAgent,

  deviceType() {
    const mobile = /Android|webOS|iPhone|iPad|BlackBerry|IEMobile|Opera Mini/i.test(this.userAgent);
    
    if (mobile) {
      return "mobile";
    } else {
      return "desktop";
    }
  },

  isIE() {
    return (this.userAgent.indexOf('MSIE ') > 0 || this.userAgent.indexOf('Trident/') > 0 || this.userAgent.indexOf('Edge/') > 0);
  }
}

export default states;
