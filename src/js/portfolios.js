import filter from './modules/filter';

function portfolios() {
  filter.init();
}

export default portfolios();
