import $ from 'jquery';
import Flickity from 'flickity';

const areasSlider = {
  init() {
    this.state = {
      activeIndex: 0,
      translateCount: 0,
      translateValue: 0,
    };
    this.DOM = {};
    this.DOM.slider = document.querySelector('.js-areas-slider');
    this.DOM.prevBtn = document.querySelector('.js-areas-prev');
    this.DOM.nextBtn = document.querySelector('.js-areas-next');
    this.DOM.items = document.querySelectorAll('.js-areas-item');
    this.DOM.nav = document.querySelector('.js-areas-nav');
    this.DOM.controls = this.DOM.nav.querySelectorAll('.js-areas-control');

    this.navTop = $(this.DOM.nav).position().top;
    this.navCellHeight = $(this.DOM.controls[0]).height();
    this.navHeight = $(this.DOM.nav).height();

    this.slider = new Flickity(this.DOM.slider, {
      cellAlign: 'left',
      prevNextButtons: false,
      pageDots: false,
      wrapAround: false,
    });

    this.DOM.prevBtn.addEventListener('click', () => this.slider.previous());
    this.DOM.nextBtn.addEventListener('click', () => this.slider.next());

    this.DOM.controls.forEach((el, index) => {
      el.addEventListener('click', () => this.slider.select(index));
    });

    this.slider.on('select', this.setActiveEls.bind(this));
    this.setActiveEls(this.state.activeIndex);
  },

  setActiveEls(index) {
    if (this.DOM.items[this.state.activeIndex].classList.contains('is-active')) {
      this.DOM.items[this.state.activeIndex].classList.remove('is-active');
    }
    if (this.DOM.controls[this.state.activeIndex].classList.contains('is-active')) {
      this.DOM.controls[this.state.activeIndex].classList.remove('is-active');
    }

    this.DOM.items[index].classList.add('is-active');
    this.DOM.controls[index].classList.add('is-active');

    const direction = index > this.state.activeIndex ? 'next' : 'prev';

    this.state.activeIndex = index;
    this.handleNavView(index);
  },

  handleNavView(index) {
    const selected = $(this.DOM.controls[index]);

    const selectedTop = selected.position().top;
    const navScrollTop = $(this.DOM.nav).scrollTop();
    const calcHeight = (this.navHeight + (this.navCellHeight * 1.5)) / 2;

    const scrollY = selectedTop + navScrollTop - calcHeight;
    
    $(this.DOM.nav).animate({
      scrollTop: scrollY
    });
  }
}

export default areasSlider;
