import $ from 'jquery';
import throttle from '../services/throttle';

const pageNav = {
  init() {
    this.state = {
      activeIndex: 0
    };
    this.DOM = {};
    this.DOM.btns = document.querySelectorAll('.js-page-btn');
    this.DOM.sections = document.querySelectorAll('.js-page-section');

    this.DOM.btns.forEach((el, index) => {
      el.addEventListener('click', () => {
        this.scrollTo(index);
      });
    });

    this.handleScroll();
    window.addEventListener('scroll', throttle(this.handleScroll.bind(this), 50));
  },

  scrollTo(index) {
    if (typeof this.DOM.sections[index] === 'undefined') return;

    const el = this.DOM.sections[index];
    const offset = document.querySelector('.js-header').getBoundingClientRect().height;
    const speed = 500;

    // jQuery animate

    // add is-scrolling class to body to hide page-nav__titles when "animating"
    // look at jquery resolve to remove is-scrolling class

    return $('html, body').animate({
      scrollTop: $(el).offset().top - offset
    }, speed).promise();
  },

  handleScroll() {
    this.DOM.sections.forEach((el, index) => {
      if (this.isInView(el)) {
        this.setActiveBtn(index);
      }
    })
  },

  isInView(el) {
    if (typeof el === 'undefined') return;

    const viewTop = 0;
    const viewBottom = viewTop + window.innerHeight;
    const elTop = el.getBoundingClientRect().top;
    const elMiddle = elTop + el.clientHeight * 0.7;

    return elMiddle <= viewBottom && elTop > viewTop;
  },

  setActiveBtn(index) {
    if (this.DOM.btns[this.state.activeIndex].classList.contains('is-active')) {
      this.DOM.btns[this.state.activeIndex].classList.remove('is-active');
    }

    this.DOM.btns[index].classList.add('is-active');
    this.state.activeIndex = index;
  },
};

export default pageNav;
