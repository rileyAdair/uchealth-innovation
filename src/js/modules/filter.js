import Isotope from 'isotope-layout';

const filter = {
  init() {
    this.filters = {};
    this.DOM = {};
    this.DOM.grid = document.querySelector('.js-projects-grid');
    this.DOM.selects = document.querySelectorAll('.js-select');
    this.DOM.checkbox = document.querySelector('.js-checkbox');
    this.DOM.groups = document.querySelectorAll('[data-filter-group]');
    this.DOM.options = document.querySelectorAll('[data-filter]');

    // Init isotope
    if (this.DOM.grid !== null) {
      this.isotope = new Isotope(this.DOM.grid, {
        itemSelector: '.js-project'
      });
    }

    // Handle filtering on page load and popstate
    this.handleUrl(this.DOM.groups);
    window.addEventListener('popstate', () => this.handleUrl(this.DOM.groups));

    // Handle select change
    this.DOM.selects.forEach(el => {
      el.onchange = () => {
        const selected = el[el.selectedIndex];
        const filterValue = selected.dataset.filter;
        const filterGroup = selected.parentNode.dataset.filterGroup;
  
        this.setFilters(filterValue, filterGroup);
        const values = this.concatValues(this.filters);
        this.filterItems(values);
        this.pushState(this.getPath());
      }
    });

    // Handle checkbox change
    this.DOM.checkbox.onchange = () => {
      const filterValue = this.DOM.checkbox.checked ? 'open' : '';
      const filterGroup = this.DOM.checkbox.dataset.filterGroup;

      this.setFilters(filterValue, filterGroup);
      const values = this.concatValues(this.filters);
      this.filterItems(values);
      this.pushState(this.getPath());
    };
  },

  setFilters(value, group) {
    if (typeof value === 'undefined') {
      this.filters[group] = '';
    } else {
      this.filters[group] = value;
    }
  },

  concatValues(obj) {
    if (obj.date && !obj.date.includes('date-')) {
      obj.date = `date-${obj.date}`;
    }

    let value = '';
    for (var prop in obj) {
      value += (obj[prop] ? '.' : '') + obj[prop];
    }
    return value;
  },

  filterItems(filter) {
    this.isotope.arrange({
      filter: filter
    });

    this.setActiveSelections();
  },

  setActiveSelections() {
    this.resetSelections();

    for (var prop in this.filters) {
      if (this.filters[prop] === 'open') {
        this.DOM.checkbox.checked = true;
      } else if (this.filters[prop]) {
        const optionEl = document.querySelector(`[data-filter=${this.filters[prop]}]`)
        optionEl.selected = true;
      }
    }
  },

  resetSelections() {
    this.DOM.checkbox.checked = false;
    this.DOM.options.forEach(option => option.selected = false);
  },

  getPath() {
    let size = 0;
    for (const prop in this.filters) {
      if (this.filters[prop]) size++;
    }

    let path = size ? `${window.location.pathname}?` : window.location.pathname;

    if (this.filters.cat) {
      path += `cat=${this.filters.cat}`;
      if (this.filters.date || this.filters.open) {
        path += '&';
      }
    }

    if (this.filters.date) {
      const date = this.filters.date.replace('date-', '');
      path += `date=${date}`;
      if (this.filters.open) {
        path += `&`;
      }
    }

    if (this.filters.open) {
      path += 'open';
    }

    return path;
  },

  pushState(path) {
    history.pushState({}, '', path);
  },

  handleUrl(groups) {
    groups.forEach(el => {
      const filterGroup = el.dataset.filterGroup;
      const filterValue = this.getUrlParameter(filterGroup);
      if (filterValue) this.setFilters(filterValue, filterGroup);
      else this.setFilters('', filterGroup);
    })

    if (window.location.search.includes('open')) this.setFilters('open', 'open');

    const values = this.concatValues(this.filters);

    this.filterItems(values);
  },

  getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }
};

export default filter;
