const stickyHeader = {
  init() {
    this.state = {
      isSticky: false
    };

    this.offsetTop = document.querySelector('.js-header').offsetTop;
    window.onscroll = () => this.handleSticky();
  },

  handleSticky() {
    if (window.pageYOffset >= this.offsetTop && this.state.isSticky === false) {
      this.state.isSticky = true;
      document.body.classList.add('is-sticky');
    } else if (window.pageYOffset < this.offsetTop && this.state.isSticky === true) {
      this.state.isSticky = false;
      document.body.classList.remove('is-sticky');
    }
  },
};

export default stickyHeader;
