const youtubePlayer = {
  init() {
    this.state = {
      isReady: false
    };
    this.DOM = {};
    this.DOM.modal = document.querySelector('.js-video-modal');
    this.DOM.playBtn = document.querySelector('.js-video-play');
    this.DOM.player = this.DOM.modal.querySelector('.js-video-player');
    this.DOM.closeBtn = this.DOM.modal.querySelector('.js-video-close');
    
    const videoID = this.DOM.player.dataset.videoid;

    const ytPromise = new Promise((resolve) => {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      window.onYouTubeIframeAPIReady = () => resolve(window.YT);
    });

    ytPromise.then((YT) => {
      this.player = new YT.Player(this.DOM.player, {
        videoId: videoID,
        height: '100%',
        width: '100%',
        events: {
          onReady: this.onReady.bind(this),
          onStateChange: this.onStateChange.bind(this)
        }
      });
    });

    this.DOM.playBtn.addEventListener('click', () => this.onPlayClick());
    this.DOM.closeBtn.addEventListener('click', () => this.onCloseClick());
  },

  onReady() {
    this.state.isReady = true;
    this.DOM.playBtn.style.opacity = 1;
  },

  onStateChange(e) {
    switch (e.data) {
      case YT.PlayerState.ENDED:
        this.hideModal();
        break;
      default:
    }
  },

  onPlayClick() {
    if (!this.state.isReady) return;
    this.player.playVideo();
    this.DOM.modal.classList.add('is-active');
    setTimeout(() => { this.DOM.modal.style.opacity = 1; });
  },

  onCloseClick() {
    this.hideModal();
    this.player.stopVideo();
  },

  hideModal() {
    this.DOM.modal.style.opacity = 0;
    setTimeout(() => { this.DOM.modal.classList.remove('is-active'); }, 400);
  }
}

export default youtubePlayer;
