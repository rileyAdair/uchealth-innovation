export default {
  init() {
    this.DOM = {};
    this.DOM.btn = document.querySelector('.js-toggle-btn');
    this.DOM.menu = document.querySelector('.js-menu-container');

    this.DOM.btn.addEventListener('click', this.toggle.bind(this));
  },

  toggle() {
    this.DOM.btn.classList.toggle('is-active');
    this.DOM.menu.classList.toggle('is-active');
  }
};
