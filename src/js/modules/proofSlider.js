import Flickity from 'flickity';

const proofSlider = {
  init() {
    this.state = {
      activeIndex: 0
    };
    this.DOM = {};
    this.DOM.slider = document.querySelector('.js-proof-slider');
    this.DOM.points = document.querySelectorAll('.js-point');

    this.slider = new Flickity(this.DOM.slider, {
      cellAlign: 'left',
      wrapAround: true,
      arrowShape: {
        x0: 10,
        x1: 53, y1: 38,
        x2: 55, y2: 35,
        x3: 15
      }
    });

    this.slider.on('change', this.setActivePoint.bind(this));
    this.setActivePoint(this.state.activeIndex);
  },

  setActivePoint(index) {
    if (this.DOM.points[this.state.activeIndex].classList.contains('is-active')) {
      this.DOM.points[this.state.activeIndex].classList.remove('is-active');
    }

    this.DOM.points[index].classList.add('is-active');
    this.state.activeIndex = index;
  }
};

export default proofSlider;
