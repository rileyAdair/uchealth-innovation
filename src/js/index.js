import '../scss/index.scss';
import states from './services/states';
import menu from './modules/menu';
import pageNav from './modules/pageNav';

// IE11 Nodelist forEach support
(function() {
  if (typeof NodeList.prototype.forEach === "function")
    return false;
  else
    NodeList.prototype.forEach = Array.prototype.forEach;
})();

function app() {
  menu.init();

  if (document.querySelector('.page-nav') !== null) {
    pageNav.init();
  }

  if (document.body.classList.contains('home')) {
    require('./home');
  };

  if (document.body.classList.contains('post-type-archive-portfolios')) {
    require('./portfolios');
  };
}

document.addEventListener('DOMContentLoaded', app);
