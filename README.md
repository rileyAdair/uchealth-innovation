# Webpack Starter - WordPress

A light webpack starter for front-end projects.

### Installation

```
npm install
```

### Start Development Server

```
npm start
```

Webpack will watch for file changes.

### Build Production Version

```
npm run build
```

The output of the build will be exposed inside the **public** folder.

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)
